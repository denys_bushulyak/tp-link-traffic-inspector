<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Traffic</title>

    <link rel="stylesheet" href="/css/app.css">

</head>

<body>

<table>
    @foreach($devices as $device)
        <tr>
            <td>{{ $device->name }}</td>
            <td><small>{{ $device->mac }}</small></td>
            <td>{{ bcdiv($device->bytes , pow(1024,2) ) }} of {{ bcdiv($device->limit , pow(1024,2) ) }} MB</td>
            <td>{{ $device->enabled ? 'On' : 'Off' }}</td>
            <td>{{ $device->reset > 0 ? "reset (".$device->reset.")" : null }}</td>
            <td>({{ $device->bytes }} bytes)</td>
        </tr>
    @endforeach
</table>

    <!-- Mainly scripts -->
<script src="/js/jquery-2.1.1.js"></script>
<script src="/js/jquery-ui-1.10.4.min.js"></script>
<script src="/js/jquery-ui.custom.min.js"></script>

</body>

</html>
