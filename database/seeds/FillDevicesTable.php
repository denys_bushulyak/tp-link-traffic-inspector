<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FillDevicesTable extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		DB::table( 'devices' )->insert( [
			[
				"name"    => "Denys (phone)",
				"mac"     => "20-62-74-A7-10-88",
				"limit"   => 0.5*pow(1024,3),
				'enabled' => 0
			],
			[
				"name"    => "Anna",
				"mac"     => "BC-EE-7B-D4-F4-6E",
				"limit"   => 0.5*pow(1024,3),
				'enabled' => 1
			],
			[
				"name"    => "Asus X301",
				"mac"     => "DC-85-DE-5B-3E-8B",
				"limit"   => 0.5*pow(1024,3),
				'enabled' => 1
			],
			[
				"name"    => "Sasha",
				"mac"     => "20-62-74-9C-60-A8",
				"limit"   => 2*pow(1024,3),
				"enabled" => 1,
			],
			[
				"name"    => "Tanya",
				"mac"     => "AC-9E-17-79-9C-CA",
				"limit"   => 3*pow(1024,3),
				"enabled" => 1
			],
			[
				"name"    => 'MacBook Pro',
				"mac"     => '',
				"limit"   => 5*pow(1024,3),
				"enable"  => 1
			]
		] );
	}
}
