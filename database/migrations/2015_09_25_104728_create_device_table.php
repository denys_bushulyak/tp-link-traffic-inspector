<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create( 'devices', function ( Blueprint $table ) {

			$table->increments( 'id' );
			$table->string( 'name' )->default( 'No name' );
			$table->string( 'mac' );
			$table->unsignedInteger( 'bytes' )->default( 0 );
			$table->unsignedInteger( 'limit' )->default( 512 * pow( 1024, 2 ) );
			$table->unsignedInteger( 'reset' )->nullable();
			$table->boolean( 'enabled' )->default( false );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop( 'devices' );
	}
}
