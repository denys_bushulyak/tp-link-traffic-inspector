<?php
/**
 * Created by PhpStorm.
 * User: Denys
 * Date: 25.09.2015
 * Time: 19:07
 */

namespace App;


use Illuminate\Support\Facades\DB;

class Devices {

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function getById( $id ) {
		return DB::table('devices')->where('id',$id)->first();
	}

	public static function getByMac( $mac ) {
		return DB::table('devices')->where('mac',$mac)->first();
	}


}