<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Scheduler;
use Illuminate\Support\Facades\DB;

$app->get( '/', function () use ( $app ) {

	Scheduler::run();

	$data = [ 'devices' => DB::table( 'devices' )->get() ];

	return view( 'dashboard', $data );
} );

$app->get('cron',function(){
	Scheduler::run();
});