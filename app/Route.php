<?php

namespace App;

use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Denys
 * Date: 25.09.2015
 * Time: 14:42
 */
class Route {

	public static function resetDeviceStatistics( $macAddress ) {

		$devices = self::getStatistics();

		$device = collect($devices)->keyBy('mac')->get($macAddress);

		if(!$device){
			Log::notice("Device with mac: " . $macAddress . " can't be reset because routed didn't return it.");
			return false;
		}

		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/SystemStatisticRpm.htm?resetone=' . $device['idAtRouter'] . '&interval=5&autoRefresh=0&Num_per_page=5&Goto_page=1&sortType=1&Num_per_page=5&Goto_page=1' );

		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		curl_exec( $ch );

		return curl_getinfo( $ch, CURLINFO_HTTP_CODE ) == 200;
	}

	/**
	 * Returns devices traffic statistic at format:<br>
	 * [
	 *      [ ip, mac, bytes ],
	 *      [ ip, mac, bytes ],
	 *      ...
	 * ]
	 *
	 * @return DeviceFromRouter[]
	 */
	public static function getStatistics() {

		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/SystemStatisticRpm.htm' );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );

		$page = curl_exec( $ch );

		$page = mb_split( '\n', $page );

		// Filter only devices.
		// Trying to know where list of devices ends;
		foreach ( $page as $index => $p ) {
			if ( str_contains( $p, [ '0,0 );', '</SCRIPT>' ] ) ) {
				break;
			}
		}

		$devices = array_only( $page, range( 2, $index - 1 ) );

		foreach ( $devices as $index => $row ) {
			$matches = [ ];
			mb_ereg( '(\d+),"([\d]{1,3}.[\d]{1,3}.[\d]{1,3}.[\d]{1,3})","([\w-]{17})",\d+,(\d+)', $row, $matches );
			$devices[ $index ] = (array) new DeviceFromRouter( array_values( array_except( $matches, [ 0 ] ) ) );
		}

		return $devices;
	}

	public static function blockDevice( $macAddress ) {

		// Request order list from router because here list changing.
		// http://192.168.0.1/userRpm/LanMacFilterRpm.htm
		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/LanMacFilterRpm.htm' );

		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		$response = curl_exec( $ch );

		$lines = mb_split( '\n', $response );

		// Filter only devices.
		// Trying to know where list of devices ends;
		foreach ( $lines as $index => $row ) {
			if ( str_contains( $row, [ '0,0 );', '</SCRIPT>' ] ) ) {
				break;
			}
		}

		$lines = array_values( array_only( $lines, range( 2, $index - 1 ) ) );

		$parsedDevicesFromSecurityList = collect();

		for ( $i = 0; $i < count( $lines ) / 3; $i ++ ) {
			$name = trim( $lines[ $i * 3 + 1 ], ',\'"' );
			$mac  = trim( $lines[ $i * 3 ], ',\'"' );

			$parsedDevicesFromSecurityList->push( DeviceFromRouter::create( $i, null, $mac, null, $name ) );
		}

		$device = $parsedDevicesFromSecurityList->keyBy( 'mac' )->get( $macAddress );

		if ( ! $device ) {
			return true; //Return true because router list of allowing did't have this device and this make it blocked automatically.
		}
		// IMPORTANT!
		// --- Advanced Settings --- / Security / Firewall / Default MAC Address Filtering Rules: Allow these PCs with enabled rules to access the Internet
		//http://192.168.0.1/userRpm/LanMacFilterRpm.htm?Mac=20-62-74-A7-10-88&Desc=Denys+Windows+Phone&State=0&Changed=1&SelIndex=0&Page=1&Save=Save    Block

		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/LanMacFilterRpm.htm?Mac=' . $device->mac . '&Desc=' . urlencode( $device->name ) . '&State=0&Changed=1&SelIndex=' . $device->idAtRouter . '&Page=1&Save=Save' );

		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		curl_exec( $ch );

		return curl_getinfo( $ch, CURLINFO_HTTP_CODE ) == 200;
	}

	public static function unblockDevice( $macAddress ) {

		// Request order list from router because here list changing.
		// http://192.168.0.1/userRpm/LanMacFilterRpm.htm
		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/LanMacFilterRpm.htm' );

		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		$response = curl_exec( $ch );

		$lines = mb_split( '\n', $response );

		// Filter only devices.
		// Trying to know where list of devices ends;
		foreach ( $lines as $index => $row ) {
			if ( str_contains( $row, [ '0,0 );', '</SCRIPT>' ] ) ) {
				break;
			}
		}

		$lines = array_values( array_only( $lines, range( 2, $index - 1 ) ) );

		$devices = collect();

		for ( $i = 0; $i < count( $lines ) / 3; $i ++ ) {
			$name = trim( $lines[ $i * 3 + 1 ], ',\'"' );
			$mac  = trim( $lines[ $i * 3 ], ',\'"' );

			$devices->push( DeviceFromRouter::create( $i, null, $mac, null, $name ) );
		}

		$device = $devices->keyBy( 'mac' )->get( $macAddress );

		// IMPORTANT!
		// --- Advanced Settings --- / Security / Firewall / Default MAC Address Filtering Rules: Allow these PCs with enabled rules to access the Internet
		//http://192.168.0.1/userRpm/LanMacFilterRpm.htm?Mac=20-62-74-A7-10-88&Desc=Denys+Windows+Phone&State=1&Changed=1&SelIndex=0&Page=1&Save=Save    UnBlock

		$ch = curl_init( 'http://' . env( 'ROUTER_IP' ) . '/userRpm/LanMacFilterRpm.htm?Mac=' . $device->mac . '&Desc=' . urlencode( $device->name ) . '&State=1&Changed=1&SelIndex=' . $device->idAtRouter . '&Page=1&Save=Save' );

		curl_setopt( $ch, CURLOPT_USERPWD, env( 'ROUTER_USERNAME' ) . ':' . env( 'ROUTER_PASSWORD' ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		curl_exec( $ch );

		return curl_getinfo( $ch, CURLINFO_HTTP_CODE ) == 200;
	}
}