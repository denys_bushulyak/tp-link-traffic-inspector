<?php
/**
 * Created by PhpStorm.
 * User: Denys
 * Date: 25.09.2015
 * Time: 22:10
 */

namespace App;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Scheduler {

	public static function run() {

		$devices = collect( DB::table( 'devices' )->get() )->keyBy( 'mac' );

		$devicesFromRouter = collect( Route::getStatistics() )->keyBy( 'mac' );

		// Grab data from router
		DB::transaction( function () use ( $devicesFromRouter, $devices ) {

			foreach ( $devicesFromRouter as $mac => $router ) {

				if ( ! isset( $devices[ $mac ] ) ) {
					continue;
				}

				$device = $devices[ $mac ];
				$router = (object) $router;

				/*
				 * Ситуация: на роутере число меньше чем в базе:
				 */
				if ( $device->bytes > $router->bytes ) {

					/*
					 * Если reset > 0 - значит такое уже случалось.
					 */
					if ( $device->reset > 0 ) {

						/*
						 * На роутере байтов больше чем в reset переменной - это нормально.
						 * Потому что теперь поле reset выступает полем сравнения разницы, чтобы узнать сколько использовано трафика.
						 */
						if ( $router->bytes >= $device->reset ) {
							$device->bytes += $router->bytes - $device->reset;
							$device->reset = (int) $router->bytes;
						}

						/*
						 * Если байтов на роутере меньше чем в reset, значит снова произошёл сброс.
						 * Jбнуляем reset до значения на роутере, а сами байты роутера добавляем непосредственно к статистике устройства.
						 */
						elseif($router->bytes < $device->reset) {
							$device->bytes += $router->bytes;
							$device->reset = (int) $router->bytes;
						}
					}

					/*
					 * Если reset == 0 - роутер сбросил данные в первый раз за отчётный период
					 */

					elseif($device->reset == 0){
						$device->reset = (int) $router->bytes;
						$device->bytes += $router->bytes;
					}
				}

				/*
				 * Всё нормально: роутер имеет чуть больше байтов чем базе на устройстве
				 */
				elseif($router->bytes >= $device->bytes) {
					$device->bytes += ( $router->bytes - $device->bytes );
				}


				DB::table( 'devices' )->where( 'mac', $mac )->update( [
					'reset' =>( int) $device->reset,
					'bytes' => (int) $device->bytes
				] );
			}
		} );


		// Making decision what to do
		DB::transaction( function () use ( $devices ) {

			foreach ( $devices as $device ) {

				$block = false;

				if ( $device->bytes >= $device->limit ) {
					Log::info( "Device: " . $device->mac . '(' . $device->name . ') blocked by traffic reason.' );

					$block = true;
				}

				if ( ! $device->enabled ) {
					Log::info( "Device: " . $device->mac . '(' . $device->name . ') blocked by device state in DB.' );

					$block = true;
				}

				if ( $block ) {
					if ( Route::blockDevice( $device->mac ) ) {
						DB::table( 'devices' )->where( 'mac', $device->mac )->update( [ 'enabled' => false ] );
					} else {
						Log::error( "Block device with mac " . $device->mac . " error." );
					}

				}
			}

		} );
	}
}