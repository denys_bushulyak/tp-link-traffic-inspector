<?php
/**
 * Created by PhpStorm.
 * User: Denys
 * Date: 25.09.2015
 * Time: 15:09
 */

namespace App;


/**
 * @property  mac
 * @property  ip
 * @property  bytes
 * @property  idAtRouter
 */
class DeviceFromRouter {
	
	/**
	 * DeviceFromRouter constructor.
	 */
	public function __construct( $data ) {

		if($data) {

			$this->idAtRouter = $data[ 0 ];
			$this->ip         = $data[ 1 ];
			$this->mac        = $data[ 2 ];
			$this->bytes      = $data[ 3 ];
			if ( isset( $data[ 4 ] ) ) {
				$this->name = $data[ 4 ];
			}
		}
	}

	public static function create( $idAtRouter, $ip, $mac, $bytes, $name ) {
		$instance = new static([]);

		$instance->idAtRouter = $idAtRouter;
		$instance->ip = $ip;
		$instance->mac = $mac;
		$instance->bytes = $bytes;
		$instance->name = $name;

		return $instance;
	}
}